package com.cf.demortc;

import android.Manifest;
import android.app.AlertDialog;
import android.content.pm.PackageManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.webrtc.AudioSource;
import org.webrtc.AudioTrack;
import org.webrtc.Camera1Enumerator;
import org.webrtc.Camera2Enumerator;
import org.webrtc.CameraEnumerator;
import org.webrtc.EglBase;
import org.webrtc.IceCandidate;
import org.webrtc.Logging;
import org.webrtc.MediaConstraints;
import org.webrtc.MediaStream;
import org.webrtc.PeerConnection;
import org.webrtc.PeerConnectionFactory;
import org.webrtc.RendererCommon;
import org.webrtc.SdpObserver;
import org.webrtc.SessionDescription;
import org.webrtc.StatsReport;
import org.webrtc.SurfaceViewRenderer;
import org.webrtc.VideoCapturer;
import org.webrtc.VideoRenderer;
import org.webrtc.VideoSource;
import org.webrtc.VideoTrack;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class MainActivity extends AppCompatActivity implements AppRTCClient.SignalingEvents, PeerConnectionClient.PeerConnectionEvents {
    private static final String TAG = "";
    private static final String LOG_TAG = "CallActivity";
    private PeerConnectionFactory factory;
    private VideoSource videoSource;
    AudioSource audioSource;
    private MediaConstraints videoConstraints;
    MediaConstraints audioConstraints;
    private MediaConstraints sdpConstraints;
    private VideoTrack localVideoTrack;
    AudioTrack localAudioTrack;
    private VideoRenderer.Callbacks localRender;
    private Button btnStart;
    private Button btnCall;
    private Button btnHang;
    private Button btnSwitchCamera;
    private Button btnPick;
    private boolean isFrontCamera = false;
    private VideoCapturer videoCapturer;
    private PeerConnection localPeer, remotePeer;
    private VideoRenderer localRenderer;
    private VideoRenderer remoteRenderer;
    private SurfaceViewRenderer localVideoView;
    private SurfaceViewRenderer remoteVideoView;
    private MediaStream mediaStream;
    AudioTrack remoteAudioTrack;
    EditText edtRoomId;
    AppRTCClient.RoomConnectionParameters roomConnectionParameters;
    PeerConnectionClient peerConnectionClient;
    PeerConnectionClient.PeerConnectionParameters peerConnectionParameters;
    AppRTCClient appRtcClient;
    private long callStartedTimeMs;
    private Toast logToast;
    private AppRTCClient.SignalingParameters signalingParameters;
    private boolean iceConnected;
    private boolean isError;
    private boolean activityRunning;
    private AppRTCAudioManager audioManager;
    private EglBase rootEglBase;
    private List<VideoRenderer.Callbacks> remoteRenderers;
    Ringtone ringtone;
    Socket socket;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnStart = (Button) findViewById(R.id.btnStart);
        btnCall = (Button) findViewById(R.id.btnCall);
        btnHang = (Button) findViewById(R.id.btnHangUp);
        btnPick = (Button) findViewById(R.id.btnPick);
        btnSwitchCamera = (Button) findViewById(R.id.btnSwitchCamera);

        // Request Permission cho Camera va record Audio
        if (ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.RECORD_AUDIO)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                    Manifest.permission.RECORD_AUDIO)) {

            } else {
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.RECORD_AUDIO, Manifest.permission.CAMERA},
                        1);
            }
        }

        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                start();
            }
        });
        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                call();
            }
        });
        btnHang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hangup();
//                disconnect();
            }
        });
        btnPick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pick();
            }
        });
        btnSwitchCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//
//                try {
//                    videoCapturer.stopCapture();
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
                switchCamera();
                btnCall.setEnabled(true);
            }
        });

        localVideoView = (SurfaceViewRenderer) findViewById(R.id.sfView); //You need define your UI element to render video
        remoteVideoView = (SurfaceViewRenderer) findViewById(R.id.sfRemoteView); //You need define your UI element to render video

        rootEglBase = EglBase.create();
        localVideoView.init(rootEglBase.getEglBaseContext(), null);
        remoteVideoView.init(rootEglBase.getEglBaseContext(), null);

        btnStart.setEnabled(false);
        btnCall.setEnabled(true);
//        peerConnectionParameters = PeerConnectionClient.PeerConnectionParameters.createDefault();
//
//        // Create connection client. Use DirectRTCClient if room name is an IP otherwise use the
//        // standard WebSocketRTCClient.
        // If capturing format is not specified for screencapture, use screen resolution.
//        appRtcClient = new WebSocketRTCClient(this);
//        roomConnectionParameters = new AppRTCClient.RoomConnectionParameters("https://appr.tc", String.valueOf(edtRoomId.getText()), false);
//        peerConnectionClient = PeerConnectionClient.getInstance();
//        peerConnectionClient.createPeerConnectionFactory(this, peerConnectionParameters, this);

        if (!PeerConnectionFactory.initializeAndroidGlobals(MainActivity.this, true, true, true)) {
            Toast.makeText(this, "Error create PeerConnection", Toast.LENGTH_SHORT).show();
        }
        PeerConnectionFactory.Options options = new PeerConnectionFactory.Options();
        factory = new PeerConnectionFactory(options);

        //Create MediaConstraints - Will be useful for specifying video and audio constraints.
        audioConstraints = new MediaConstraints();
        videoConstraints = new MediaConstraints();

        switchCamera();

//        videoSource = factory.createVideoSource(videoCapturer);
//        videoCapturer.startCapture(720/*width*/, 1200/*height*/, 25/*fps*/);
//        localVideoTrack = factory.createVideoTrack("100", videoSource);
//
//        //create an AudioSource instance
//        audioSource = factory.createAudioSource(audioConstraints);
//        localAudioTrack = factory.createAudioTrack("101", audioSource);
//        localAudioTrack.setEnabled(true);
//
//        localVideoView.setScalingType(RendererCommon.ScalingType.SCALE_ASPECT_FIT);
//        localVideoView.setMirror(true);
//        localVideoTrack.setEnabled(true);
//        localRenderer = new VideoRenderer(localVideoView);
//        localVideoTrack.addRenderer(localRenderer);
    }

    public void start() {

    }

    public void call() {
        btnHang.setEnabled(true);
        btnCall.setEnabled(false);
        btnStart.setEnabled(false);
//        btnPick.setEnabled(true);


//        startCall();

        PeerConnection.IceServer ics = new PeerConnection.IceServer("stun:stun.ideasip.com");
        List<PeerConnection.IceServer> iceServers = new ArrayList<>();
        iceServers.add(ics);
        PeerConnection.RTCConfiguration rtcConfig = new PeerConnection.RTCConfiguration(iceServers);
        // TCP candidates are only useful when connecting to a server that supports
        // ICE-TCP.
        rtcConfig.tcpCandidatePolicy = PeerConnection.TcpCandidatePolicy.DISABLED;
        rtcConfig.bundlePolicy = PeerConnection.BundlePolicy.MAXBUNDLE;
        rtcConfig.rtcpMuxPolicy = PeerConnection.RtcpMuxPolicy.REQUIRE;
        rtcConfig.continualGatheringPolicy = PeerConnection.ContinualGatheringPolicy.GATHER_CONTINUALLY;
        // Use ECDSA encryption.
        rtcConfig.keyType = PeerConnection.KeyType.ECDSA;

        sdpConstraints = new MediaConstraints();
        sdpConstraints.mandatory.add(new MediaConstraints.KeyValuePair("OfferToReceiveAudio", "true"));
        sdpConstraints.mandatory.add(new MediaConstraints.KeyValuePair("offerToReceiveVideo", "true"));
        localPeer = factory.createPeerConnection(rtcConfig, sdpConstraints, new CustomPeerConnectionObserver("localPeerCreation") {
            @Override
            public void onIceCandidate(IceCandidate iceCandidate) {
                super.onIceCandidate(iceCandidate);
                onIceCandidateReceived(localPeer, iceCandidate);
            }
        });

        //creating remotePeer
        remotePeer = factory.createPeerConnection(rtcConfig, sdpConstraints, new CustomPeerConnectionObserver("remotePeerCreation") {

            @Override
            public void onIceCandidate(IceCandidate iceCandidate) {
                super.onIceCandidate(iceCandidate);
                onIceCandidateReceived(remotePeer, iceCandidate);
            }

            public void onAddStream(MediaStream mediaStream) {
                super.onAddStream(mediaStream);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        btnPick.setEnabled(true);
                        btnPick.setBackgroundResource(R.drawable.button_call);
                        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
                        ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
//                        ringtone.play();
                    }
                });
                gotRemoteStream(mediaStream);
            }

            @Override
            public void onIceGatheringChange(PeerConnection.IceGatheringState iceGatheringState) {
                super.onIceGatheringChange(iceGatheringState);
                Log.d("onIceGatheringChange", iceGatheringState.toString());
            }
        });

        // Đưa video track vào MediaStream
        // Once we have that, we can create our VideoTrack
        // Note that VIDEO_TRACK_ID can be any string that uniquely
        // identifies that video track in your application
        // false is value to start camera capture
//        videoCapturer = createCameraCapturer(new Camera1Enumerator(false));
        mediaStream = factory.createLocalMediaStream("102");
        mediaStream.addTrack(localAudioTrack);
        mediaStream.addTrack(localVideoTrack);
        localPeer.addStream(mediaStream);


        //creating Offer
        localPeer.createOffer(new CustomSdpObserver("localCreateOffer") {
            @Override
            public void onCreateSuccess(SessionDescription sessionDescription) {
                //we have localOffer. Set it as local desc for localpeer and remote desc for remote peer.
                //try to create answer from the remote peer.
                super.onCreateSuccess(sessionDescription);
                String subType = "offer";
                sessionDescription.toString();
                Log.d("CallTest", "Offer Type: " + sessionDescription.type);
                try {
                    JSONObject sessionObject = new JSONObject();
                    JSONObject obj = new JSONObject();
                    obj.put("key", "He9Y3AA7xtVQahaKGuon5HYSAqy1");
                    obj.put("from", "He9Y3AA7xtVQahaKGuon5HYSAqy1");
                    obj.put("to", "NGTLlnAReRfhfdD9Gir5L5Dme3T2");
//                    obj.put("to", "26kgT2OLkMSyk4mQTXXstrZppg32");
                    obj.put("time", "");
                    obj.put("type", "signal");
                    obj.put("content", sessionDescription.toString());
                    obj.put("subtype", subType);
                    obj.put("conversationId", "He9Y3AA7xtVQahaKGuon5HYSAqy1-NGTLlnAReRfhfdD9Gir5L5Dme3T2");
                    socket.emit("chat message", obj);


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                localPeer.setLocalDescription(new CustomSdpObserver("localSetLocalDesc"), sessionDescription);
                remotePeer.setRemoteDescription(new CustomSdpObserver("remoteSetRemoteDesc"), sessionDescription);
            }
        }, sdpConstraints);
    }

    private void switchCamera(){
        if(isFrontCamera){
            isFrontCamera = false;
        }else{
            isFrontCamera = true;
        }

        if(videoCapturer != null){
            try {
                videoCapturer.stopCapture();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        videoCapturer = createCameraCapturer(new Camera1Enumerator(false));
        videoSource = factory.createVideoSource(videoCapturer);
        videoCapturer.startCapture(720/*width*/, 1200/*height*/, 25/*fps*/);
        localVideoTrack = factory.createVideoTrack("100", videoSource);

        //create an AudioSource instance
        audioSource = factory.createAudioSource(audioConstraints);
        localAudioTrack = factory.createAudioTrack("101", audioSource);
        localAudioTrack.setEnabled(true);

        localVideoView.clearImage();
        localVideoView.setScalingType(RendererCommon.ScalingType.SCALE_ASPECT_FIT);
        localVideoView.setMirror(true);
        localVideoTrack.setEnabled(true);

        localRenderer = new VideoRenderer(localVideoView);
        localVideoTrack.addRenderer(localRenderer);

    }

    private void pick() {
        ringtone.stop();

        remotePeer.createAnswer(new CustomSdpObserver("remoteCreateOffer") {
            @Override
            public void onCreateSuccess(SessionDescription sessionDescription) {
                //remote answer generated. Now set it as local desc for remote peer and remote desc for local peer.
                super.onCreateSuccess(sessionDescription);
                String subType = "answer";
                sessionDescription.toString();
                Log.d("CallTest", "Offer Type: " + sessionDescription.type);
                try {

                    JSONObject obj = new JSONObject();
                    obj.put("key", "NGTLlnAReRfhfdD9Gir5L5Dme3T2");
                    obj.put("from", "NGTLlnAReRfhfdD9Gir5L5Dme3T2");
                    obj.put("to", "He9Y3AA7xtVQahaKGuon5HYSAqy1");
//                    obj.put("to", "26kgT2OLkMSyk4mQTXXstrZppg32");
                    obj.put("time", "");
                    obj.put("type", "signal");
                    obj.put("content", sessionDescription.toString());
                    obj.put("subtype", subType);
                    obj.put("conversationId", "He9Y3AA7xtVQahaKGuon5HYSAqy1-NGTLlnAReRfhfdD9Gir5L5Dme3T2");
                    socket.emit("chat message", obj);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                remotePeer.setLocalDescription(new CustomSdpObserver("remoteSetLocalDesc"), sessionDescription);
                localPeer.setRemoteDescription(new CustomSdpObserver("localSetRemoteDesc"), sessionDescription);

            }
        },new MediaConstraints());
    }

    private void startCall() {
        callStartedTimeMs = System.currentTimeMillis();

        // Start room connection.
        logAndToast("Đang kết nối tới " + roomConnectionParameters.roomUrl);
        appRtcClient.connectToRoom(roomConnectionParameters);

        // Create and audio manager that will take care of audio routing,
        // audio modes, audio device enumeration etc.
        audioManager = AppRTCAudioManager.create(this);
        // Store existing audio settings and change audio mode to
        // MODE_IN_COMMUNICATION for best possible VoIP performance.
        Log.d(LOG_TAG, "Starting the audio manager...");
        audioManager.start(this::onAudioManagerDevicesChanged);
    }

    // This method is called when the audio manager reports audio device change,
    // e.g. from wired headset to speakerphone.
    private void onAudioManagerDevicesChanged(
            final AppRTCAudioManager.AudioDevice device, final Set<AppRTCAudioManager.AudioDevice> availableDevices) {
        Log.d(LOG_TAG, "onAudioManagerDevicesChanged: " + availableDevices + ", "
                + "selected: " + device);
        // TODO(henrika): add callback handler.
    }

    public void onIceCandidateReceived(PeerConnection peer, IceCandidate iceCandidate) {
        //we have received ice candidate. We can set it to the other peer.
        if (peer == localPeer) {
            remotePeer.addIceCandidate(iceCandidate);
        } else {
            localPeer.addIceCandidate(iceCandidate);
        }
    }

    private void gotRemoteStream(MediaStream stream) {
        //we have remote video stream. add to the renderer.
        final VideoTrack videoTrack = stream.videoTracks.getFirst();
        if (stream.audioTracks.size() > 0) {
            remoteAudioTrack = stream.audioTracks.getFirst();
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    remoteRenderer = new VideoRenderer(remoteVideoView);
                    remoteVideoView.setVisibility(View.VISIBLE);
                    videoTrack.addRenderer(remoteRenderer);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    // Setup listener lắng nghe sự kiện tắt cuộc gọi, chuyển camera và bật tắt micro
//    private void setupListeners() {
//        binding.buttonCallDisconnect.setOnClickListener(view -> onCallHangUp());
//
//        binding.buttonCallSwitchCamera.setOnClickListener(view -> onCameraSwitch());
//
//        binding.buttonCallToggleMic.setOnClickListener(view -> {
//            boolean enabled = onToggleMic();
//            binding.buttonCallToggleMic.setAlpha(enabled ? 1.0f : 0.3f);
//        });
//    }

    private void hangup() {
        localPeer.close();
        remotePeer.close();
        localPeer = null;
        remotePeer = null;
        btnStart.setEnabled(false);
        btnCall.setEnabled(true);
        btnHang.setEnabled(false);
    }

    private VideoCapturer createCameraCapturer(CameraEnumerator enumerator) {
        final String[] deviceNames = enumerator.getDeviceNames();
        // First, try to find front facing camera
        Log.d(TAG, "Looking for front facing cameras.");
        if (isFrontCamera) {
            for (String deviceName : deviceNames) {
                if (enumerator.isFrontFacing(deviceName)) {
                    Log.d(TAG, "Creating front facing camera capturer.");
                    VideoCapturer videoCapturer = enumerator.createCapturer(deviceName, null);
                    if (videoCapturer != null) {
                        return videoCapturer;
                    }
                }
            }
        } else {
            for (String deviceName : deviceNames) {
                if (enumerator.isBackFacing(deviceName)) {
                    Log.d(TAG, "Creating front facing camera capturer.");
                    VideoCapturer videoCapturer = enumerator.createCapturer(deviceName, null);
                    if (videoCapturer != null) {
                        return videoCapturer;
                    }
                }
            }
        }

        // Front facing camera not found, try something else
        Log.d(TAG, "Looking for other cameras.");
        for (String deviceName : deviceNames) {
            if (!enumerator.isFrontFacing(deviceName)) {
                Log.d(TAG, "Creating other camera capturer.");
                VideoCapturer videoCapturer = enumerator.createCapturer(deviceName, null);
                if (videoCapturer != null) {
                    return videoCapturer;
                }
            }
        }
        return null;
    }

    // Log |msg| and Toast about it.
    private void logAndToast(String msg) {
        Log.d(LOG_TAG, msg);
        if (logToast != null) {
            logToast.cancel();
        }
        logToast = Toast.makeText(this, msg, Toast.LENGTH_SHORT);
        logToast.show();
    }

    // Should be called from UI thread
    private void callConnected() {
        final long delta = System.currentTimeMillis() - callStartedTimeMs;
        Log.i(LOG_TAG, "Call connected: delay=" + delta + "ms");
        if (peerConnectionClient == null || isError) {
            Log.w(LOG_TAG, "Call is connected in closed or error state");
            return;
        }
        // Update video view.
        updateVideoView();
        // Enable statistics callback.
        peerConnectionClient.enableStatsEvents(true, 1000);
    }

    private void updateVideoView() {
//        binding.remoteVideoLayout.setPosition(REMOTE_X, REMOTE_Y, REMOTE_WIDTH, REMOTE_HEIGHT);
//        binding.remoteVideoView.setScalingType(SCALE_ASPECT_FILL);
//        binding.remoteVideoView.setMirror(false);
//
//        if (iceConnected) {
//            binding.localVideoLayout.setPosition(
//                    LOCAL_X_CONNECTED, LOCAL_Y_CONNECTED, LOCAL_WIDTH_CONNECTED, LOCAL_HEIGHT_CONNECTED);
//            binding.localVideoView.setScalingType(SCALE_ASPECT_FIT);
//        } else {
//            binding.localVideoLayout.setPosition(
//                    LOCAL_X_CONNECTING, LOCAL_Y_CONNECTING, LOCAL_WIDTH_CONNECTING, LOCAL_HEIGHT_CONNECTING);
//            binding.localVideoView.setScalingType(SCALE_ASPECT_FILL);
//        }
//        binding.localVideoView.setMirror(true);
//
//        binding.localVideoView.requestLayout();
//        binding.remoteVideoView.requestLayout();
    }

    private void reportError(final String description) {
        runOnUiThread(() -> {
            if (!isError) {
                isError = true;
                disconnectWithErrorMessage(description);
            }
        });
    }

    private void disconnectWithErrorMessage(final String errorMessage) {
        if (!activityRunning) {
            Log.e(LOG_TAG, "Critical error: " + errorMessage);
            disconnect();
        } else {
            new AlertDialog.Builder(this)
                    .setTitle("Call error")
                    .setMessage(errorMessage)
                    .setCancelable(false)
                    .setNeutralButton("OK",
                            (dialog, id) -> {
                                dialog.cancel();
                                disconnect();
                            })
                    .create()
                    .show();
        }
    }

    private boolean useCamera2() {
        return Camera2Enumerator.isSupported(this);
    }

    private boolean captureToTexture() {
        return true;
    }

    private VideoCapturer createVideoCapturer() {
        VideoCapturer videoCapturer;
        if (useCamera2()) {
            Logging.d(LOG_TAG, "Creating capturer using camera2 API.");
            videoCapturer = createCameraCapturer(new Camera2Enumerator(this));
        } else {
            Logging.d(LOG_TAG, "Creating capturer using camera1 API.");
            videoCapturer = createCameraCapturer(new Camera1Enumerator(captureToTexture()));
        }
        if (videoCapturer == null) {
            reportError("Failed to open camera");
            return null;
        }
        return videoCapturer;
    }

    // Disconnect from remote resources, dispose of local resources, and exit.
    private void disconnect() {
        activityRunning = false;
        if (appRtcClient != null) {
            appRtcClient.disconnectFromRoom();
            appRtcClient = null;
        }
        if (peerConnectionClient != null) {
            peerConnectionClient.close();
            peerConnectionClient = null;
        }
        localVideoView.release();
        remoteVideoView.release();
        if (audioManager != null) {
            audioManager.stop();
            audioManager = null;
        }
        if (iceConnected && !isError) {
            setResult(RESULT_OK);
        } else {
            setResult(RESULT_CANCELED);
        }
        finish();
    }

    // PeerConnection Events
    @Override
    public void onLocalDescription(final SessionDescription sdp) {
        final long delta = System.currentTimeMillis() - callStartedTimeMs;
        runOnUiThread(() -> {
            if (appRtcClient != null) {
                logAndToast("Sending " + sdp.type + ", delay=" + delta + "ms");
                if (signalingParameters.initiator) {
                    appRtcClient.sendOfferSdp(sdp);
                } else {
                    appRtcClient.sendAnswerSdp(sdp);
                }
            }
            if (peerConnectionParameters.videoMaxBitrate > 0) {
                Log.d(LOG_TAG, "Set video maximum bitrate: " + peerConnectionParameters.videoMaxBitrate);
                peerConnectionClient.setVideoMaxBitrate(peerConnectionParameters.videoMaxBitrate);
            }
        });
    }

    @Override
    public void onIceCandidate(final IceCandidate candidate) {
        runOnUiThread(() -> {
            if (appRtcClient != null) {
                appRtcClient.sendLocalIceCandidate(candidate);
            }
        });
    }

    @Override
    public void onIceCandidatesRemoved(final IceCandidate[] candidates) {
        runOnUiThread(() -> {
            if (appRtcClient != null) {
                appRtcClient.sendLocalIceCandidateRemovals(candidates);
            }
        });
    }

    @Override
    public void onIceConnected() {
        final long delta = System.currentTimeMillis() - callStartedTimeMs;
        runOnUiThread(() -> {
            logAndToast("ICE connected, delay=" + delta + "ms");
            iceConnected = true;
            callConnected();
        });
    }

    @Override
    public void onIceDisconnected() {
        runOnUiThread(() -> {
            logAndToast("ICE disconnected");
            iceConnected = false;
            disconnect();
        });
    }

    @Override
    public void onPeerConnectionClosed() {
    }

    @Override
    public void onPeerConnectionStatsReady(final StatsReport[] reports) {
        runOnUiThread(() -> {
        });
    }

    @Override
    public void onPeerConnectionError(final String description) {
        reportError(description);
    }

    // Signalling events
    private void onConnectedToRoomInternal(final AppRTCClient.SignalingParameters params) {
        final long delta = System.currentTimeMillis() - callStartedTimeMs;

        signalingParameters = params;
        logAndToast("Creating peer connection, delay=" + delta + "ms");
        VideoCapturer videoCapturer = null;
        if (peerConnectionParameters.videoCallEnabled) {
            videoCapturer = createVideoCapturer();
        }
        peerConnectionClient.createPeerConnection(rootEglBase.getEglBaseContext(), localVideoView,
                remoteRenderers, videoCapturer, signalingParameters);

        if (signalingParameters.initiator) {
            logAndToast("Creating OFFER...");
            // Create offer. Offer SDP will be sent to answering client in
            // PeerConnectionEvents.onLocalDescription event.
            peerConnectionClient.createOffer();
        } else {
            if (params.offerSdp != null) {
                peerConnectionClient.setRemoteDescription(params.offerSdp);
                logAndToast("Creating ANSWER...");
                // Create answer. Answer SDP will be sent to offering client in
                // PeerConnectionEvents.onLocalDescription event.
                peerConnectionClient.createAnswer();
            }
            if (params.iceCandidates != null) {
                // Add remote ICE candidates from room.
                for (IceCandidate iceCandidate : params.iceCandidates) {
                    peerConnectionClient.addRemoteIceCandidate(iceCandidate);
                }
            }
        }
    }

    @Override
    public void onConnectedToRoom(final AppRTCClient.SignalingParameters params) {
        runOnUiThread(() -> onConnectedToRoomInternal(params));
    }

    @Override
    public void onRemoteDescription(final SessionDescription sdp) {
        final long delta = System.currentTimeMillis() - callStartedTimeMs;
        runOnUiThread(() -> {
            if (peerConnectionClient == null) {
                Log.e(LOG_TAG, "Received remote SDP for non-initilized peer connection.");
                return;
            }
            logAndToast("Received remote " + sdp.type + ", delay=" + delta + "ms");
            peerConnectionClient.setRemoteDescription(sdp);
            if (!signalingParameters.initiator) {
                logAndToast("Creating ANSWER...");
                // Create answer. Answer SDP will be sent to offering client in
                // PeerConnectionEvents.onLocalDescription event.
                peerConnectionClient.createAnswer();
            }
        });
    }

    @Override
    public void onRemoteIceCandidate(final IceCandidate candidate) {
        runOnUiThread(() -> {
            if (peerConnectionClient == null) {
                Log.e(LOG_TAG, "Received ICE candidate for a non-initialized peer connection.");
                return;
            }
            peerConnectionClient.addRemoteIceCandidate(candidate);
        });
    }

    @Override
    public void onRemoteIceCandidatesRemoved(final IceCandidate[] candidates) {
        runOnUiThread(() -> {
            if (peerConnectionClient == null) {
                Log.e(LOG_TAG, "Received ICE candidate removals for a non-initialized peer connection.");
                return;
            }
            peerConnectionClient.removeRemoteIceCandidates(candidates);
        });
    }

    @Override
    public void onChannelClose() {
        runOnUiThread(() -> {
            logAndToast("Remote end hung up; dropping PeerConnection");
            disconnect();
        });
    }

    @Override
    public void onChannelError(final String description) {
        reportError(description);
    }
}
